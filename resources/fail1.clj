(in-ns 'user)

(declare fail1)

(when-not (bound? #'fail1)
  (throw (ex-info "Expected fail1 to be bound!" {:fail1 :fail})))
