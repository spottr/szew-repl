(ns ^{:author "Sławek 'smg' Gwizdowski"
      :doc "REPL utilities - tests."}
 szew.repl-test
  (:require [szew.repl :refer [nrepl-stop! nrepl-start! do-want?
                               get-log-level! set-log-level!]]
            [clojure.tools.logging :as log]
            [clojure.test :refer [deftest testing is use-fixtures]]))

(use-fixtures :once (fn [f]
                      ;; We'd like nREPL to be stopped
                      (nrepl-stop!)
                      (f)
                      ;; To be really, really stopped.
                      (nrepl-stop!)))

(deftest So-tell-me-what-you-want-what-you-really-really-dowant?
  (is (every? true? (mapv do-want? ["ENABLED" "enable"
                                    "M'KAY" "mkay"
                                    "K." "k" "OK" "OK."
                                    "D'OH" "duh"
                                    "PLEASE" "pls"
                                    "ya" "yeah" "yes" "yup" "aye"
                                    "go" "sure"])))
  (is (every? false? (mapv do-want? ["DISABLED" "disable"
                                     "NOPE" "don't"
                                     "NO" "plsno"
                                     "nay"]))))

(deftest Starting-and-cycling-nREPL-defaults-testing
  (testing "Basic server lifecycle"
    (try
      (let [server  (nrepl-start!)
            port1   (:port server)]
        (is (integer? port1))
        (is (= (-> server :server-socket .getInetAddress .getAddress vec)
               [127 0 0 1]))
        (is (re-matches #"^localhost.*$"
                        (-> server :server-socket .getInetAddress .getHostName)))
        (is (thrown? Exception (nrepl-start!))) ;; server present?
        (let [_ (nrepl-stop!)
              server2 (nrepl-start!)]
          (is (integer? (:port server2)))
          (is (= (:port server2) port1))
          (is (= (-> server2 :server-socket .getInetAddress .getAddress vec)
                 [127 0 0 1]))
          (is (not= server server2))))
      (catch java.net.BindException _
        (log/warn "Bind: Address/port in use. Skipping default nREPL test.")))))

(deftest Getting-and-setting-logger-level
  (is (nil? (get-log-level! "unknown-logger")))
  (is (= "ALL" (str (set-log-level! "unknown-logger" :all))))
  (is (= "ALL" (str (get-log-level! "unknown-logger"))))
  (is (= Integer/MIN_VALUE (.toInt (get-log-level! "unknown-logger"))))
  (is (= "OFF" (str (set-log-level! "unknown-logger" "OFF"))))
  (is (= "OFF" (str (get-log-level! "unknown-logger"))))
  (is (= Integer/MAX_VALUE (.toInt (get-log-level! "unknown-logger"))))
  (is (= "ALL" (str (set-log-level! "unknown-logger" Integer/MIN_VALUE))))
  (is (= "ALL" (str (get-log-level! "unknown-logger"))))
  (is (= "OFF" (str (set-log-level! "unknown-logger" Integer/MAX_VALUE))))
  (is (= "OFF" (str (get-log-level! "unknown-logger"))))
  (is (= "DEBUG" (str (get-log-level!))))  ;; test profile!
  (is (= "WARN" (str (set-log-level! :warn))))
  (is (= "WARN" (str (get-log-level!))))
  (is (thrown? Exception (set-log-level! :x))))
