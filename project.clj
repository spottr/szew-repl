(defproject szew/repl "0.3.11-SNAPSHOT"

  :description "Simple Clojure REPL seeder."

  :license {:name "MIT Public License"
            :distribution :repo
            :comments "LICENSE file in project root directory."}

  :dependencies [[org.clojure/clojure "1.12.0"]
                 ;; Environment variables in Clojure
                 [environ "1.2.0"]
                 ;; For vim-fireplace and rest.
                 [nrepl "1.3.0"]
                 ;; Logging infrastructure!
                 [ch.qos.logback/logback-classic "1.5.15"]
                 [ch.qos.logback/logback-core "1.5.15"]
                 [org.slf4j/slf4j-api "2.0.16"]
                 [org.slf4j/log4j-over-slf4j "2.0.16"]
                 [org.clojure/tools.logging "1.3.0"]]

  :profiles {:dev {:dependencies [[criterium "0.4.6"]]
                   :plugins [[lein-auto "0.1.3"]
                             [lein-codox "0.10.8"]
                             [lein-environ "1.2.0"]]
                   :source-paths ["dev"]
                   :env {}}
             :1.10 {:dependencies [[org.clojure/clojure "1.10.3"]]}
             :1.11 {:dependencies [[org.clojure/clojure "1.11.4"]]}
             :uberjar {:aot :all
                       :env {}}}

  :plugins [[lein-environ "1.1.0"]]

  :main szew.repl

  :aot [clojure.tools.logging.impl ;; aot or die!!!1
        szew.repl]

  :codox {;:metadata {:doc/format :markdown}
          :project {:name "szew/repl"}
          :namespaces [#"^szew\.repl.*$"]
          :doc-files ["CHANGELOG.md"]
          ;; that was fun:
          :source-uri ~(str "https://bitbucket.org/spottr/szew-repl/src/"
                            "112ace46e892cc73ce04206db60bb6afe32fca5f" ; 0.3.10
                            "/{filepath}#{basename}-{line}")}

  :repl-options {:timeout 120000
                 :init-ns user}

  ;; file masks for lein-auto
  :auto {:default {:file-pattern #"\.(clj|cljs|cljc)$"}}

  ;; doing the n33dful
  :aliases {"test!" ["do" "clean," "compile," "test"]
            "pack!" ["do" "clean," "compile," "uberjar"]
            ;"prep!" ["do" "test!," "marg"]
            "autotest" ["auto" "test!"]}

  ;:global-vars {*warn-on-reflection* true}

  :jar-exclusions [#"logback-test\.xml"
                   #"fail\d.clj"
                   #"ok\d.clj"]
  :omit-source false
  :target-path "target/"
  )
