(ns ^{:author "Sławek 'smg' Gwizdowski"
      :doc "REPL setup and script reloading.

We are holding state here. Read the source."}
 szew.repl
  (:require [clojure.repl :refer [pst]]
            [clojure.main]
            [nrepl.server]
            [environ.core :refer [env]]
            [clojure.tools.logging :as log]
            [clojure.java.io :as java.io])
  (:import [ch.qos.logback.classic Level]
           [ch.qos.logback.classic Logger]
           [org.slf4j LoggerFactory]
           [java.util Properties])
  (:gen-class))

;; usage: (do-want? (:dry-run env "nope!"))

(def do-want?
  "What do we see as ENABLE."
  (comp boolean
        (partial re-matches
                 #"(?xi)(?:enabled?
                 |m'?kay
                 |o?k\.?
                 |d'?(?:o|u)h
                 |please
                 |pls
                 |y(?:a+|e+)
                 |yeah
                 |yes
                 |ya+s!?
                 |yup
                 |aye
                 |go
                 |sure
                 )")))

(def default-bind
  "Default BIND for nREPL."
  "127.0.0.1")

(def default-port
  "Default PORT for nREPL."
  "55555")

;;
;; ## Hello state!
;;

(def scripts
  "Program arguments to evaluate before the REPL starts."
  (atom nil))

(def script-errors
  "Map of script path to Exception faced during load."
  (atom {}))

(def nrepl-server
  "Singleton (dirty word!) holding the nREPL server."
  (atom nil))

(defn raze-scripts!
  "Remove all SCRIPTS and SCRIPT-ERRORS."
  []
  (reset! script-errors {})
  (reset! scripts nil))

(defn push-scripts!
  "Get ARGS sequence and append them to `scripts`."
  [args]
  (swap! scripts (fn [current]
                   (if (empty? current)
                     (vec args)
                     (into (vec current) args)))))

(defn oops
  "Throwable helper, borrowed from Stuart Halloways Repl Driven Development."
  [^Throwable x]
  (log/error "Exception:" (.getMessage x))
  (log/info "Details: (clojure.repl/pst *e), ex-data?"
            (if (nil? (ex-data x)) "No.  ;_;" "Yes!  ^_^")))

(defn fails
  "Get list of files that had errors in previous execution."
  []
  (filterv (partial contains? @script-errors) @scripts))

(defn err
  "Get last traceback for file."
  ([a-script]
   (get @script-errors a-script))
  ([]
   (second (first @script-errors))))

(defn pest
  "Everybody needs love sometimes."
  []
  (let [last-err (err)]
    (when last-err
      (pst last-err))))

(defn load-script!
  "Load single script. True if success, false if Exception."
  [a-file]
  (log/info "Running:" a-file)
  (try (load-file a-file)
       (swap! script-errors dissoc a-file)
       (log/info "Success:" a-file)
       true
       (catch Exception e
         (swap! script-errors assoc a-file e)
         false)))

(defn load-scripts!
  "Execute args (default to @scripts), keep exceptions in @script-errors."
  ([args]
   (when (seq args)
     (io! "Bad things happen to good states."
          (log/info "Executing files.")
          (loop [todo args]
            (when (seq todo)
              (let [f (first todo)]
                (if-not (load-script! f)
                  (throw (err f))
                  (recur (rest todo))))))
          (log/info "... and now for something completely interactive!"))))
  ([]
   (swap! script-errors (constantly {}))
   (load-scripts! @scripts)))

(letfn [(nss []
          (->> *ns*
               (ns-aliases)
               (mapv (juxt (comp str second) first))
               (into (hash-map))))]
  (defn ?
    "Prints present namespace aliases."
    ([]
     (let [lut (nss)]
       (if (seq lut)
         (do (println "Present namespaces aliases:")
             (doseq [[k v] (sort-by first lut)]
               (println (format "%28s\t%s" k v))))
         (println "No namespace aliases!"))))
    ([nsn]
     (let [lut  (nss)
           rlut (->> lut (mapv (juxt second first)) (into {}))]
       (if-let [nom (if (contains? lut (str nsn)) (str nsn)
                      (when (contains? rlut nsn) (get rlut nsn)))]
         (do (println nom (get lut nom))
             (doseq [sym (sort-by str (keys (ns-publics (symbol nom))))]
               (println (format "\t%s/%s" (get lut nom "?") sym))))
         (println (format "Sorry, don't know \"%s\"." nsn)))))))

(defn nuke-ns!
  "Very unkindly unmap everything from current namespace."
  ([skip-set]
   (log/info "Nuking:" (.name *ns*))
   (doseq [[k _] (ns-publics *ns*)
           :when (not (contains? skip-set k))]
     (log/info "Unmapping:" k)
     (ns-unmap *ns* k))
   (log/info "Nuked:" (.name *ns*))
   :nuked!)
  ([]
   (nuke-ns! #{})))

(defn !!
  "Re-loads all input scripts, just alias for (load-scripts!)."
  []
  (load-scripts! @scripts))

(defn !?
  "Re-loads only inputs scripts from last failure."
  []
  (load-scripts! (drop-while #(not (contains? @script-errors %)) @scripts)))

(defn ?!
  "Re-loads only inputs scripts from last failure. Calls (!?)."
  []
  (!?))

;; nREPL

(defn nrepl-stop!
  "Try to deinit the server."
  []
  (swap! nrepl-server
         (fn [server]
           (when server
             (try (nrepl.server/stop-server server)
                  (catch Exception ex
                    (log/debug "Failed to stop nREPL server:" ex))))
           nil)))

(defn nrepl-start!
  "Try to init the server."
  ([{:keys [bind port] :as args}]
   (swap! nrepl-server
          (fn [server]
            (if server
              (throw (ex-info "Server present?" {:args args :server server}))
              (nrepl.server/start-server :bind bind :port port)))))
  ([]
   (nrepl-start! {:bind default-bind :port (Integer/parseInt default-port)})))

(defn nrepl-display!
  "Display nREPL BIND and PORT as nrepl://BIND:PORT through log/info."
  []
  (let [server @nrepl-server]
  (when server
    (log/info (format "nrepl://%s:%d"
                      (-> server
                          :server-socket
                          (.getInetAddress)
                          (.getHostAddress))
                      (:port server))))))

(defn nrepl-cycle!
  "Create nREPL instance. See nrepl-start! for args."
  ([args]
   (nrepl-stop!)
   (nrepl-start! args)
   (nrepl-display!))
  ([]
   (let [bind (get env :nrepl-bind default-bind)
         port (Integer/parseInt (get env :nrepl-port default-port))]
     (nrepl-cycle! {:bind bind :port port}))))

(defn exit
  "Exit VM. Shuts down agents too."
  ([] (exit 0))
  ([code]
   (shutdown-agents)
   (System/exit code)))

;; Logging

(defn get-log-level!
  "Return Level of requested logger String."
  ([]
   (get-log-level! Logger/ROOT_LOGGER_NAME))
  ([logger]
   (.getLevel ^Logger (LoggerFactory/getLogger logger))))

(defn set-log-level!
  "Set log level for String logger, returns Level set.

  Accepted levels: :all, :trace, :debug, :info, :warn, :error, :off

  Hint: logback doesn't do :fatal, so we won't either. Default logger is ROOT.

  Also does number and String levels, using Level/toLevel heuristics."
  ([level]
   (set-log-level! Logger/ROOT_LOGGER_NAME level))
  ([logger level]
   (let [lo ^Logger (LoggerFactory/getLogger logger)]
     (cond
       (number? level)
       (.setLevel lo (Level/toLevel (int level)))
       (string? level)
       (.setLevel lo (Level/toLevel (str level)))
       (= :all level)
       (.setLevel lo Level/ALL)
       (= :trace level)
       (.setLevel lo Level/TRACE)
       (= :debug level)
       (.setLevel lo Level/DEBUG)
       (= :info level)
       (.setLevel lo Level/INFO)
       (= :warn level)
       (.setLevel lo Level/WARN)
       (= :error level)
       (.setLevel lo Level/ERROR)
       (= :off level)
       (.setLevel lo Level/OFF)
       :else
       (throw (ex-info "Unknown level!" {:level level})))
     (.getLevel lo))))

;; Misc

(defn get-version
  "Try to extract artifact version from META-INF."
  [dep]
  (let [path (str "META-INF/maven/" (or (namespace dep) (name dep))
                  "/" (name dep) "/pom.properties")
        props (java.io/resource path)]
    (when props
      (with-open [stream (java.io/input-stream props)]
        (let [props (doto (Properties.) (.load stream))]
          (.getProperty props "version"))))))

(defn repl-me!
  "Seed some basic REPL utilities."
  []
  (clojure.core/require '[clojure.core :refer :all]
                        '[clojure.repl :refer :all]
                        '[clojure.pprint :refer [pprint print-table]]
                        '[environ.core :refer [env]]
                        '[clojure.tools.logging :as log]
                        '[szew.repl
                          :refer [set-log-level! get-log-level!
                                  ? !! !? nuke-ns! exit get-version
                                  nrepl-start! nrepl-stop!
                                  nrepl-cycle! nrepl-display!
                                  fails err pest help]])
  :done)

(defn help
  "Print some basic information."
  []
  (println)
  (println "Welcome to szew.repl! Things to see and do here:")
  (println " * (?) to list present namespaces and aliases,")
  (println " * (!!) to re-execute all scripts again,")
  (println " * (!?) to re-execute only failed scripts,")
  (println " * (nuke-ns!) to clean the namespace up!")
  (println)
  (println "Current namespace was set up with:")
  (clojure.repl/source szew.repl/repl-me!)
  (println))

;; main!

(defn -main
  "Entry point for standalone operation.

  Note: this is not a var, this is a java method!"
  [& args]
  (set-log-level! (get env :log-level "INFO"))
  (log/info "Log level set to:" (str (get-log-level!)))
  (push-scripts! args)
  (log/info (format "szew/repl: %s" (get-version 'szew/repl)))
  (log/info (format "nrepl: %s" (get-version 'nrepl)))
  (when (do-want? (get env :nrepl-on "nope"))
    (nrepl-cycle!))
  (log/info "First time here? Try: (help)")
  (clojure.main/repl :init (fn []
                             (in-ns (symbol (get env :repl-namespace "user")))
                             (repl-me!)
                             (!!))
                     :caught oops))
