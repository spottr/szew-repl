# szew/repl

Simple Clojure REPL seeder.

[![szew/repl](https://clojars.org/szew/repl/latest-version.svg)](https://clojars.org/szew/repl)

[API documentation][apicodox] | [Changelog][changelog]

_Please note: API breakage will happen in minor versions (0.X.0) before 1.0.0
is released. Only patch versions (0.n.X) are safe until then._

** Table of Contents **

[TOC]

---

## Why

I've been dogfooding my private Clo*j*ure toolbox (named `szew`) since 2012.

Splitting out and releasing non-proprietary parts.

## Usage

Add water to seed a very basic **R**ead-**E**val-**P**rint-**L**oop! Includes
logging and nREPL server. Simple use:

1. Call `szew.repl/-main` with paths to Clo*j*ure files.
2. Main will `load-file` them in order.
3. You get into a REPL when done or on Exception. In `'user`.
4. You can exit with `(szew.repl/exit)`.

There's only one namespace here, `szew.repl`. [It holds some helpers][szew-repl],
most of which will be pulled in the default REPL namespce.

Running `(szew.repl/repl-me!)` propagates current namespace with `clojure.core`,
`clojure.repl` and `szew.repl`. By default it happens in the entry namespace,
be it user selected (see below) or the default `'user`.

If exception happens during file loading -- you get a REPL. You can try to fix
and reload all files with `(!!)` or pick up from the failed one via `(!?)`.

Running `(?)` shows you all aliased namespaces, while `(? 'alias)` is similar
to `(dir alias)`.

You can get some of that from `(help)`.

Logging based on *logback*, see `get-log-level!` and `set-log-level!`.

Your scripts are in `scripts`, Exception in `script-errors`.

With `(nuke-ns!)` you can quickly `ns-unmap` a bunch of things.

## Alternative Usage

Uberjar packed with goodies, with `:main szew.repl`. Then just:

```bash
rlwrap java -jar uberjar.jar path/script1.clj path/script2.clj
```

And find yourself at that friendly `user=>`. With logging and nREPL.

## Env

Environment variables are used for some features.

### nREPL

Starting single standalone nREPL server within `-main` is done with:

```bash
export NREPL_ON = please

export NREPL_BIND = localhost      [default: 127.0.0.1]

export NREPL_PORT = 45678          [default: 55555]
```

### Logging

No default is set for root logger.

```bash
export LOG_LEVEL = WARN            [default: INFO]
```

### Namespace

Default namespace can be changed with:

```bash
export REPL_NAMESPACE = mynamespace  [default: user]
```

To land on REPL-ready `mynamespace=>` prompt.

## License

Copyright © 2012-2024 Sławek Gwizdowski

MIT License, text can be found in the LICENSE file.

[apicodox]: http://spottr.bitbucket.io/szew-repl/latest/
[szew-repl]: https://spottr.bitbucket.io/szew-repl/latest/szew.repl.html
[changelog]: CHANGELOG.md
