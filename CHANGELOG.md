# Change Log
All notable changes to this project will be documented in this file.
This change log follows the conventions of [keepachangelog.com](https://keepachangelog.com/).

## Wishlist

### Add

* Setting log pattern dynamically for given logger, as in PatternSample example:
  https://logback.qos.ch/manual/layouts.html

## [Unreleased]
### Changes

## [0.3.10] - 2024-08-14

### Fixes

* Welcome message said to run `(help)`, but it was not present.

### Adds

* Now importing `clojure.tools.looging :as log` into default namespace.
* Now lists contents of `repl-me!` in `(help)` invocation, showing requires.

## [0.3.9] - 2024-08-13

### Changes

* Dependencies upgrade: Clojure, SLF4J and nREPL.

## [0.3.8] - 2023-03-13

### Changes

* Dependencies upgrade: Clojure, Logback, SLF4J and nREPL.

## [0.3.7] - 2022-08-25

### Changes

* Dependencies upgrade: Clojure, Logback, SLF4J and nREPL.

## [0.3.6] - 2021-12-13

### Changes

* Dependencies upgrade, major release of nREPL.

## [0.3.5] - 2020-09-17

### Changes

* `repl-me!` now excludes `help`, includes `get-version` and `exit`.

## [0.3.4] - 2020-09-12

### Changes

* Upgrade of nREPL to 0.8.1 point release.

### Adds

* REPL_NAMESPACE environment variable for setup outside of 'user.

## [0.3.3] - 2020-03-28

### Changes

* Upgrade of nREPL version to 0.7.0 release.
* Manual testing .clj scripts excluded from the JAR.

## [0.3.2] - 2020-03-04

### Changes

* Fix `nil` handling in `szew.repl/nrepl-server` atom.

## [0.3.1] - 2019-06-18

### Changes

* Fix log level setting logic.
* Default log level fixed to INFO now.
* Upgrade Clojure to 1.10.1.

## [0.3.0] - 2019-02-28

### Changes

* Major dependency version updates: Clojure 1.10.0 and nrepl 0.6.0.

## [0.2.0] - 2018-08-07

### Changes

* Major dependency version updates: Clojure 1.9.0, nrepl 0.4.4
* Introduced `(help)` and modified welcome messages
* Extended `do-want?` and added tests for that

## [0.1.1] - 2017-10-23

### Fixes

* `szew/repl` artifact version call fixed in `-main`.

## 0.1.0 - 2017-10-23

### Added

* Initial release. Just a humble `szew.repl/-main` and stuff.

[Unreleased]: https://bitbucket.org/spottr/szew-repl/branches/compare/master%0D0.3.10#diff
[0.3.10]: https://bitbucket.org/spottr/szew-repl/branches/compare/0.3.10%0D0.3.9#diff
[0.3.9]: https://bitbucket.org/spottr/szew-repl/branches/compare/0.3.9%0D0.3.8#diff
[0.3.8]: https://bitbucket.org/spottr/szew-repl/branches/compare/0.3.8%0D0.3.7#diff
[0.3.7]: https://bitbucket.org/spottr/szew-repl/branches/compare/0.3.7%0D0.3.6#diff
[0.3.6]: https://bitbucket.org/spottr/szew-repl/branches/compare/0.3.6%0D0.3.5#diff
[0.3.5]: https://bitbucket.org/spottr/szew-repl/branches/compare/0.3.5%0D0.3.4#diff
[0.3.4]: https://bitbucket.org/spottr/szew-repl/branches/compare/0.3.4%0D0.3.3#diff
[0.3.3]: https://bitbucket.org/spottr/szew-repl/branches/compare/0.3.3%0D0.3.2#diff
[0.3.2]: https://bitbucket.org/spottr/szew-repl/branches/compare/0.3.2%0D0.3.1#diff
[0.3.1]: https://bitbucket.org/spottr/szew-repl/branches/compare/0.3.1%0D0.3.0#diff
[0.3.0]: https://bitbucket.org/spottr/szew-repl/branches/compare/0.3.0%0D0.2.0#diff
[0.2.0]: https://bitbucket.org/spottr/szew-repl/branches/compare/0.2.0%0D0.1.1#diff
[0.1.1]: https://bitbucket.org/spottr/szew-repl/branches/compare/0.1.1%0D0.1.0#diff

